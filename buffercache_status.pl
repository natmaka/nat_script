#!/usr/bin/perl
# Outputs Linux buffer cache 'instantaneous' buffercache (useful) hit ratio of a given process.

use strict;
use warnings;
use Modern::Perl '2020';
use utf8;

my $delay=1;
my $thePID;

use Getopt::Long qw(:config gnu_getopt);
GetOptions(
  'pid|p=i' => \$thePID,
  'delay|d=f' => \$delay,
) or die "getOptions error";

die "Please provide: -p PID" if (not defined $thePID);
die "There is no process ${thePID}" if (! -d "/proc/${thePID}");

{
  open my $fh, '<', "/proc/${thePID}/io" or die "Could not open the procfile: $!";

  sub get_rwb() {
    my $rchar;
    my $readbytes;

    seek($fh, 0, 0) or die "Cannot seek";
    while (my $line = <$fh>) {
      if ($line =~ /^rchar: (\d+)$/) {
        $rchar = $1;
        next;
      }
      if ($line =~ /^read_bytes: (\d+)$/) {
        $readbytes = $1;
        next;
      }
    }
    die "Cannot read rchar" if (! defined $rchar);
    die "Cannot read readbytes" if (! defined $readbytes);
    return ($rchar, $readbytes);
  }
}

my $last_rchar; my $last_readbytes;
($last_rchar, $last_readbytes)=get_rwb();
while (1) {
  my $rchar;          # bytes read by the process
  my $read_bytes;     # bytes read FROM A STORAGE LAYER by the process
  my $ratio;

  sleep $delay;
  ($rchar, $read_bytes)=get_rwb();
  my $new_read_bytes = $read_bytes - $last_readbytes;
  my $new_rchar = $rchar - $last_rchar;
  if (0 == $new_rchar) {
    $ratio = 0;
  } else {
    $ratio = $new_read_bytes / $new_rchar;
    $ratio = 1 if ($ratio > 1); # because (see 'man proc') rchar includes things such as terminal I/O
  }
	printf "$new_read_bytes %.3f\n", $ratio;
  $last_rchar=$rchar;
  $last_readbytes=$read_bytes;
}
