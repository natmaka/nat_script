# nat_script
Various Unix/Linux tools, quick hacks.

# Installation
1. Copy the script into a directory (you may want to put its name in your PATH shell environment variable).
2. The 'bash_nat_std' script must also be in this directory.
3. Edit the script, replace '/home/nat/bin/utis/bash_nat_std' with the complete path of the 'bash_nat_std' on your machine.

## Scripts

## luks_device_ok_p

Checks a LUKS device for LUKS adequation:
- LUKS version
- key derivation function (KDF), per https://mjg59.dreamwidth.org/66429.html

### Usage
Invoke it with, as a parameter, the name of the object (probably a filename beginning with /dev/ ) containing the LUKS device.

You may 'source' it in a script, its exit status is 0 if the check was successful.

## pg_pause_process
Pauses (SIGSTOP) a PostgreSQL process, and after a delay resumes it (SIGCONT).

This is only useful in order to temporarily relieve the system from some/all PG-related load, for example in order to let it finish something else (even PG's queries which are not paused).

This may produce unwanted side effects:
- queries aborting due to some timeout (PG's own 'idle_in_transaction_session_timeout')
- reduced overall (agreggated) throughput (mainly because data useful for paused queries may leave the caches during the pause, then will have to be reloaded)

### Prerequisite
The user account invoking it must be authorized to 'sudo' to the PostgreSQL's account.

### Usage
CLI arguments: PID(s).

### buffercache_status.pl

Outputs Linux buffer cache 'instantaneous' hit ratio of a given process.

Parameters: -p PID -d delay_second

To obtain the amount of bytes read from a storage layer (not the buffercache) by the process #135:
'''buffercache_status.pl -p 135 -d 5'''

Todo:
- accept more than one PID as sets of PID(s): "-p 12+55 -p 77": '12+55' means 'all reads from PID 12 and PID 55'
- for each PID set: output min, max, mean (arith...), std dev...
See also /proc/meminfo Cached # buffercache size (see 'man proc')
